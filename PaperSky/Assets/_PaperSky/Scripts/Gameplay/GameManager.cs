﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get { return instance; } }
    private static GameManager instance = null;

    public GameLevel Level { get { return level; }
        set
        {
            if (OnGameLevelChanged != null)
                OnGameLevelChanged.Invoke(value);

            LoadSceneByGameLevel(value);

            level = value;
        }
    }
    [SerializeField]
    private GameLevel level = GameLevel.PaperTown;

    public GameStateChangeEvent OnGameLevelChanged;

    public Dictionary<GameLevel, string> sceneMap = new Dictionary<GameLevel, string>();

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("Only ONE GameManager should be up and running.");
            Destroy(this);
            return;
        }

        ///////////////////////////////////////////////////////
        // WRITE HERE LEVELS NAMES PER GAMELEVEL.
        // Don't forget to add the scene in the build settings.
        sceneMap.Add(GameLevel.PaperTown, "PaperTown");
        sceneMap.Add(GameLevel.Cottage, "Cottage");
        ///////////////////////////////////////////////////////

        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        LoadSceneByGameLevel(level);
    }

    private void LoadSceneByGameLevel(GameLevel level)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneMap[level]);
    }
}

public enum GameLevel
{
    PaperTown,
    Cottage
}

[System.Serializable]
public class GameStateChangeEvent : UnityEvent<GameLevel> {}