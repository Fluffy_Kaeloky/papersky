﻿using UnityEngine;
using System.Collections;

public class StartAnimAtRandom : MonoBehaviour
{
    public Animation animator = null;

    private void Awake()
    {
        if (animator != null)
        {
            foreach (AnimationState state in animator)
                state.normalizedTime = Random.Range(0.0f, 1.0f);
        }
    }
}
