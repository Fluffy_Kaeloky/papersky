﻿using UnityEngine;
using System.Collections;

public class WindTunnel : MonoBehaviour
{
    public Vector3 direction = Vector3.up;
    public float force = 10.0f;
    public ForceMode forceMode = ForceMode.Force;

    public Transform centerTransform = null;

    public bool interpolateForceByCenter = true;
    public AnimationCurve interpolationCurve = new AnimationCurve(new Keyframe(0.0f, 1.0f), new Keyframe(1.0f, 1.0f));

    private void Start()
    {
        direction = direction.normalized;
        interpolationCurve.postWrapMode = WrapMode.ClampForever;
        interpolationCurve.preWrapMode = WrapMode.ClampForever;
    }

    private void OnTriggerStay(Collider other)
    {
        Rigidbody otherRb = other.GetComponentInParent<Rigidbody>();
        Vector3 finalForce = direction * force * Time.fixedDeltaTime;

        if (interpolateForceByCenter)
        {
            Vector3 center = transform.position;
            if (centerTransform != null)
                center = centerTransform.position;

            float distance = Vector3.Distance(center, otherRb.transform.position);
            finalForce *= interpolationCurve.Evaluate(distance);
        }

        otherRb.AddForce(finalForce, forceMode);
    }
}
