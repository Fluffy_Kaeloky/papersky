﻿using UnityEngine;
using System.Collections;

public class AnimatorParameterSetter : MonoBehaviour
{
    public Animator animator = null;

    public void SetTrigger(string name)
    {
        if (animator != null)
            animator.SetTrigger(name);
    }

    public void SetBool(string name, bool value)
    {
        if (animator != null)
            animator.SetBool(name, value);
    }

    public void SetFloat(string name, float value)
    {
        if (animator != null)
            animator.SetFloat(name, value);
    }
}
