﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class OnPlayerTriggerNotification : MonoBehaviour
{
    public UnityEvent OnPlayerTrigger = new UnityEvent();

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.root.gameObject.tag == "Player" && OnPlayerTrigger != null)
            OnPlayerTrigger.Invoke();
    }
}
