﻿using UnityEngine;
using System.Collections;

public class PlaneCamera : MonoBehaviour
{
    public Transform target = null;

    public bool autoComputeOffset = true;
    public Vector3 offset = Vector3.zero;

    public float lerpFactor = 0.5f;
    public float lerpRotationFactor = 0.5f;

    public float maxRollAngle = 15.0f;

    private void Start()
    {
        if (autoComputeOffset && target)
            offset = transform.position - target.position;
    }

	private void FixedUpdate ()
    {
        if (target)
        {
            transform.position = Vector3.Lerp(transform.position, target.position + target.TransformDirection(offset), lerpFactor * Time.deltaTime);

            Vector3 upVector = target.up;
            float angle = 0.0f;
            if ((angle = Vector3.Angle(Vector3.up, target.up)) > maxRollAngle)
                upVector = Vector3.Slerp(Vector3.up, target.up, maxRollAngle / angle);

            Quaternion targetRotation = Quaternion.LookRotation((target.position - transform.position).normalized, upVector);

            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, lerpRotationFactor * Time.deltaTime);
        }
	}
}
