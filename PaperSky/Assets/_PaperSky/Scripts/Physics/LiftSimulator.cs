﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//Airfoil lift coeficient curve : http://www.aerospaceweb.org/question/airfoils/q0150b.shtml

public class LiftSimulator : MonoBehaviour
{
    public Transform centerOfLift = null;

    [Tooltip("Since paper planes don't go really high up, we assume that the air density never changes.")]
    public float airDensity = 1.0f;
    public float liftArea = 0.1f;
    public AnimationCurve liftCurve = new AnimationCurve(new Keyframe(0.0f, 0.0f), new Keyframe(180.0f, 0.0f));

    public Text debugText = null;

    private new Rigidbody rigidbody = null;

    private void Awake()
    {
        rigidbody = GetComponentInParent<Rigidbody>();
        if (!rigidbody)
        {
            Debug.LogError("No rigidbody on parent of " + gameObject.name + ".");
            enabled = false;
        }
    }

    private void FixedUpdate()
    {
        Debug.DrawLine(transform.position, transform.position + transform.forward * 3.0f, Color.blue);

        float cross = 0.0f;

        bool invert = false;

        Vector3 projectedForward = Vector3.ProjectOnPlane(transform.forward, Vector3.Cross(rigidbody.velocity.normalized, transform.up));
        Debug.DrawLine(transform.position, transform.position + projectedForward.normalized * 3.0f);
        float angleOfAttack = Vector3.Angle(projectedForward.normalized, rigidbody.velocity.normalized);

        float evaluateSample = angleOfAttack;
        if (angleOfAttack <= 90.0f)
        {
            Vector3 transformedVelocity = transform.InverseTransformDirection(rigidbody.velocity.normalized);
            Vector3 crossVec = Vector3.Cross(Vector3.forward, transformedVelocity);

            Debug.DrawLine(transform.position, transform.position + crossVec * 300.0f, Color.cyan);
            Debug.DrawLine(transform.position, transform.position + transformedVelocity * 3.0f, Color.red);

            if ((cross = crossVec.x) < 0.0f)
                invert = true;
        }
        else
        {
            evaluateSample -= 90.0f;
            Vector3 transformedVelocity = transform.InverseTransformDirection(rigidbody.velocity.normalized);
            Vector3 crossVec = Vector3.Cross(Vector3.forward, transformedVelocity);

            Debug.DrawLine(transform.position, transform.position + crossVec * 300.0f, Color.cyan);
            Debug.DrawLine(transform.position, transform.position + transformedVelocity * 3.0f, Color.red);

            if ((cross = crossVec.x) < 0.0f)
                invert = true;
        }

        float liftCoeficient = liftCurve.Evaluate(evaluateSample);

        float liftForce = liftCoeficient * ((airDensity * Mathf.Pow(rigidbody.velocity.magnitude, 2.0f)) / 2.0f) * liftArea;

        Debug.DrawLine(transform.position, transform.position + (invert ? -1.0f : 1.0f) * transform.up * 3.0f * Mathf.Sign(liftForce), Color.yellow);
        Debug.DrawLine(transform.position, transform.position + rigidbody.velocity.normalized * 3.0f, Color.green);

        rigidbody.AddForceAtPosition((invert ? -1.0f : 1.0f) * transform.up * liftForce, centerOfLift.position, ForceMode.Force);
        
        if (debugText)
            debugText.text = "EvaluateSample : " + evaluateSample + " AngleOfAttack : " + angleOfAttack + " Lift Coeficient : " + liftCoeficient + 
                "\nVelocity : " + rigidbody.velocity.magnitude + " Lift Force : " + liftForce +
                "\nInvert : " + invert + " Cross : " + cross;
    }
}
