﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PlayerPhysicsRotator : MonoBehaviour
{
    public float torque = 0.1f;
    public float rollTorque = 0.01f;

    private float pitchInput = 0.0f;
    private float rollInput = 0.0f;

    private new Rigidbody rigidbody = null;

    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        pitchInput = Input.GetAxis("Vertical");
        rollInput = Input.GetAxis("Horizontal");
    }

    private void FixedUpdate()
    {
        rigidbody.AddTorque(transform.TransformVector(new Vector3(pitchInput * torque * Time.fixedDeltaTime, 0.0f, -rollInput * rollTorque * Time.fixedDeltaTime)), ForceMode.Force);
    }
}
