﻿using UnityEngine;
using System.Collections;

public class CustomCenterOfMass : MonoBehaviour
{
    public Transform centerOfMass = null;

    private new Rigidbody rigidbody = null;

    private void Start()
    {
        rigidbody = GetComponentInParent<Rigidbody>();
        rigidbody.centerOfMass = centerOfMass.position - rigidbody.transform.position;
    }
}
