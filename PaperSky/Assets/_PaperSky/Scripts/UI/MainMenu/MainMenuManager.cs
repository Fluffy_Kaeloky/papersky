﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class MainMenuManager : MonoBehaviour
{
    private Animator animator = null;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void NewGame()
    {
        animator.SetTrigger("NewGame");
    }

    public void Continue()
    {
        animator.SetTrigger("Continue");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    public void LoadSave()
    {
        throw new System.NotImplementedException();
    }
}
